package Logica;

/*
 * By Paco Gomez
 * Esta clase realiza el calculo
 * 
 * */
public class CalcularLetra {
	
	private String dni="";
	private int dni_numero=0;
	private char letrasDni[] = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
	private char letra=' ';
	private int restoDni = 0;
	
	//El constructor recibe el DNI en formato String
	public CalcularLetra(String dni) {
		this.dni = dni;
		if(this.dni.length()==8 && isNumeric(this.dni)){
			//pasamos el dni a numero
			this.dni_numero = Integer.parseInt(this.dni);
			//sacamos el resto para poder averiguar la letra respectiva
			this.restoDni = this.dni_numero%23;
			//sacamos la letra 
			this.letra = this.letrasDni[this.restoDni];
		}else{
			//error ya que el dni es demasiado corta o demasiado larga
			System.out.println("ERROR!!! Introduce un DNI VALIDO");
		}
	}
	
	//devolver letra
	public char devolverLetra(){
		return this.letra;
	}
	
	//Comprueba que el DNI que reciba sea un numero
	public static boolean isNumeric(String str){
	    try {
	      double d = Double.parseDouble(str);
	    }catch(NumberFormatException nfe){
	      return false;
	    }
	    return true;
	  }

}
