package Ventana;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.SwingConstants;
import Logica.CalcularLetra;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/*
 * By Paco Gomez
 * Esta ventana tendr� dos JTextFields
 * El primero recojer� el DNI
 * El segundo calcular� la letra al apretar el bot�n
 * 
 * */
public class VentanaLetraDNI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblNewLabel;
	private JTextField textField_1;
	private Component verticalGlue;
	private JButton btnCalcular;
	private CalcularLetra cl;


	/**
	 * Create the frame.
	 */
	public VentanaLetraDNI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JLabel lblInsertarDni = new JLabel("Introduce DNI: ");
		lblInsertarDni.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblInsertarDni.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblInsertarDni);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setMaximumSize(new Dimension(300,30));
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblNewLabel = new JLabel("Letra DNI Calculada");
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		contentPane.add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setMaximumSize(new Dimension(300, 30));
		textField_1.setColumns(10);
		contentPane.add(textField_1);
		
		verticalGlue = Box.createVerticalGlue();
		contentPane.add(verticalGlue);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cl = new CalcularLetra(textField.getText());
				textField_1.setText(String.valueOf(cl.devolverLetra()));
			}
		});
		btnCalcular.setAlignmentX(Component.CENTER_ALIGNMENT);
		contentPane.add(btnCalcular);
	}

}
